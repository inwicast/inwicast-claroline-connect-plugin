<?php

namespace Inwicast\ClarolinePluginBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * MediacenterUserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MediacenterUserRepository extends EntityRepository
{
}
